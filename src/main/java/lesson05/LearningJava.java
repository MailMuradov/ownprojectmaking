package lesson05;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class LearningJava {
  public static void main(String[] args) {
    // Home Java +
//    System.out.println("Hello World");

    // Intro +

    // Get Started +

    // Syntax +

    // Comments +
    // Line comments
    /* Multilines comments */

    // Variables +
//    String a = "Hello";
//    int b = 123;
//    boolean c = true;
//    float d = 5.534f;
//    double e = 4.3235;
//    char f = 'A';
//    System.out.println(a + b + c + d + e + f);

//    String name = "Mike";
//    int age = 20;
//    System.out.println(name + " is " + age);

//    int num;
//    num = 50;
//    num = 60; //now "num" is 60
//    System.out.println(num);

//    String name = "Mike ";
//    String surname = "Spike";
//    String fullName = name + surname;
//    System.out.println(fullName);
//    int x = 10;
//    int y = 5;
//    int a = 5, b = 3, c = 4;
//    System.out.println(x + y);
//    System.out.println(x * y);
//    System.out.println(x / y);
//    System.out.println(a + b + c);

    // Data types +

    // Type Casting +
//    int myInt = 10;
//    double myDouble = myInt;
//    System.out.println(myDouble);
//
//    double newDouble = 9.56;
//    int newInt = (int) newDouble;
//    System.out.println(newInt);

    // Operators -----------------------------------------------------------------------
//    int x = 10;
//    System.out.println(x > 9 && 10 < x + 1);
//    System.out.println(x > 9 || 10 < x);
//    System.out.println(!(x > 5 && 7 < x));

    // Strings +
//    String johny = "Alcapone";
//    System.out.println(johny.length());
//    System.out.println(johny.toUpperCase());
//    System.out.println(johny.toLowerCase());
//    String money = "What is he may he doin";
//    System.out.println(money.indexOf("may")); // shows place the word start (11)
//    String hello = "I wanna do \"exactly\" this";
//    System.out.println(hello);
//    String lego = "Doin\' good";
//    System.out.println(lego);

    // Math +
//    int x = 5;
//    int y = 6;
//    int z = 25;
//    int a = -5;
//    System.out.println(Math.max(x, y)); //find who is higher
//    System.out.println(Math.min(x, y));
//    System.out.println(Math.sqrt(z));
//    System.out.println(Math.abs(a));
//    System.out.println(Math.random());

    // Booleans +

    // If...Else +
//    int x = 10;
//    int y = 20;
//    int z = 20/10;
//    if (z == 3){
//      System.out.println("That is right");
//    }else{
//      System.out.println("That is not right");
//    };
//
//    int time = 11;
//    if (time < 12){
//      System.out.println("It\'s morning");
//    }else if (time < 18){
//      System.out.println("It\'s day");
//    }else{
//      System.out.println("It\'s evening");
//    }
//
//    int oclock = 18;
//    String check = (oclock < 12) ? "morning" : "evening";
//    System.out.println(check);

    // Switch +
//    int weekDay = 5;
//    switch (weekDay) {
//      case 1:
//        System.out.println("Monday");
//        break;
//      case 2:
//        System.out.println("Tuesday");
//        break;
//      case 3:
//        System.out.println("Wednesday");
//        break;
//      case 4:
//        System.out.println("Thursday");
//        break;
//      case 5:
//        System.out.println("Friday");
//        break;
//      case 6:
//        System.out.println("Saturday");
//        break;
//      case 7:
//        System.out.println("Sunday");
//        break;
//    }
//
//    String Time = "12:00";
//    switch (Time) {
//      case "11:00":
//        System.out.println("Right time");
//        break;
//      case "13:00":
//        System.out.println("Right time2");
//        break;
//      default:
//        System.out.println("No right time");

//    String str_Sample = "RockStar";
//    System.out.println("Replace 'Rock' with 'Duke': " + str_Sample.replace("Rock", "Duke"));

    // While loop
//    int check = 10;
////    while (check <= 20){
////      System.out.println(check);
////      check++;
////    }
////
////    do {
////      System.out.println(check);
////      check++;
////    }while (check<15);

    // For loop
//    for (int i = 10; i < 20; i++){
//      System.out.println(i);
//    }
//    for (int x = 10; x < 20; x = x+2){
//      System.out.println(x);
//    }
//    String[] brand = {"Apple", "Nokia", "Z-one"};
//    for (String print: brand){
//      System.out.println(print);
//    }

    // Break / Continue +
//    for (int i = 2; i < 10; i++) {
//      if (i == 4){
//        break;
//      }
//      System.out.println(i);
//    }
//    for (int i = 7; i < 15; i++) {
//      if (i == 12){
//        continue;
//      }
//      System.out.println(i);
//    }

//    int i = 20;
//    while (i < 30){
//      System.out.println(i);
//      i++;
//      if (i == 25){
//        break;
//      }
//    }

//    int i = 20;
//    while (i < 30){
//      if (i == 25){
//        i++;
//        continue;
//      }
//      System.out.println(i);
//      i++;
//    }

    // Arrays +
//    String[] cars= {"Mazda", "Volvo", "BMW"};
//    System.out.println(cars.length);
//
//    for (int i = 0; i < cars.length; i++) {
//      System.out.println(cars[i]);
//    }
//    for (String i: cars){
//      System.out.println(i);
//    }

//    int[][] numbers = {{1,2,3,4}, {5,6,7}};
//    for (int i = 0; i < numbers.length; i++) {
//      for (int j = 0; j < numbers[i].length; j++) {
//        System.out.println(numbers[i][j]);
//      }
//    }

//    ArrayList collector = new ArrayList();
//    Scanner scanner = new Scanner(System.in);
//    int numbers = scanner.nextInt();
//    collector.add("hello");
//    collector.add("hello");
//    collector.add("hello");
//    collector.add(numbers);
//    System.out.println(collector);

//    int mainOne[] = {1, 2, 3, 5, 4, 6, 7};
//    Arrays.sort(mainOne);
//    int chosen = 5;
//    System.out.println(chosen + " is found at " + Arrays.binarySearch(mainOne, chosen));
//    System.out.println(Arrays.toString(Arrays.copyOfRange(mainOne, 1, 5)));
//    Arrays.fill(mainOne, chosen);
//    System.out.println(Arrays.toString(mainOne));
//    Arrays.sort(mainOne);
//    System.out.println(Arrays.toString(mainOne));

    // User Input
//    Scanner myObject = new Scanner(System.in);
//    System.out.println("Write your name");
//    String name = myObject.nextLine();
//    System.out.println("Hello Mr." + name);

//    Scanner myScan = new Scanner(System.in);
//    System.out.println("Please write your name, age and salary");
//
//    String name = myScan.nextLine();
//    int age = myScan.nextInt();
//    double salary = myScan.nextDouble();
//
//    System.out.println("Name" + name);
//    System.out.println("Age" + age);
//    System.out.println("Salary" + salary);

  }
}
