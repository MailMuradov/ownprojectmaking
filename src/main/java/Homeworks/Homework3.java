package Homeworks;

import java.util.Arrays;
import java.util.Scanner;

public class Homework3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[][] weekDays = new String[7][2];
        weekDays[0][0] = "Sunday";
        weekDays[0][1] = "Relax";
        weekDays[1][0] = "Monday";
        weekDays[1][1] = "Go to work";
        weekDays[2][0] = "Tuesday";
        weekDays[2][1] = "Go for sports";
        weekDays[3][0] = "Wednesday";
        weekDays[3][1] = "Create a new project";
        weekDays[4][0] = "Thursday";
        weekDays[4][1] = "Update finance book";
        weekDays[5][0] = "Friday";
        weekDays[5][1] = "Go to pub";
        weekDays[6][0] = "Saturday";
        weekDays[6][1] = "Plan for next week";
        System.out.println("Welcome, please write the day of the week to check your plans.");
        System.out.println("After you want finish you schedule check just mention Exit.");
        String checkList = scanner.nextLine();
        boolean finish = true;
        while (finish){
            checkList = checkList.toLowerCase();
            switch (checkList){
                case "monday":
                    System.out.println(weekDays[1][1]);
                    checkList = scanner.nextLine();
                    break;
                case "tuesday":
                    System.out.println(weekDays[2][1]);
                    checkList = scanner.nextLine();
                    break;
                case "wednesday":
                    System.out.println(weekDays[3][1]);
                    checkList = scanner.nextLine();
                    break;
                case "thursday":
                    System.out.println(weekDays[4][1]);
                    checkList = scanner.nextLine();
                    break;
                case "friday":
                    System.out.println(weekDays[5][1]);
                    checkList = scanner.nextLine();
                    break;
                case "saturday":
                    System.out.println(weekDays[6][1]);
                    checkList = scanner.nextLine();
                    break;
                case "sunday":
                    System.out.println(weekDays[0][1]);
                    checkList = scanner.nextLine();
                    break;
                case "exit":
                    System.out.println("Getting out");
                    finish = false;
                    break;
                default:
                    System.out.println("Please write the day of the week.");
                    checkList = scanner.nextLine();
                    break;
            }
        }
    }
}
